import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class Parks extends StatefulWidget {
  @override
  _ParksState createState() => _ParksState();
}

class _ParksState extends State<Parks> {
  late Query _ref;
  Query reference =
  FirebaseDatabase.instance.reference().child('Places').orderByChild('Category').equalTo('Park');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ref =  FirebaseDatabase.instance.reference().child('Places').orderByChild('Category').equalTo('Park');
  }

  Widget _buildContactItem({required Map contact}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.all(10),
      height: 130,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                contact['Name'],
                style: TextStyle(
                    fontSize: 16,
                    color: Theme
                        .of(context)
                        .primaryColor,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                contact['Category'],
                style: TextStyle(
                    fontSize: 16,
                    color: Theme
                        .of(context)
                        .primaryColor,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                contact['Location'],
                style: TextStyle(
                    fontSize: 16,
                    color: Theme
                        .of(context)
                        .primaryColor,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              TextField(
                // onChanged: (value) => _runFilter(value),
                decoration: const InputDecoration(
                    labelText: 'Search', suffixIcon: Icon(Icons.search)),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                height: 120.0.h,
                child: FirebaseAnimatedList(
                  query: _ref,
                  itemBuilder: (BuildContext context, DataSnapshot snapshot,
                      Animation<double> animation, int index) {
                    Map contact = snapshot.value;
                    contact['key'] = snapshot.key;
                    return _buildContactItem(contact: contact);
                  },
                ),
              ),
            ],

          ),
        ),
      ),
    );
  }
}

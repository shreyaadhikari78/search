import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:search_items/widgets/appbar.dart';
import 'package:search_items/categories/restaurants.dart';
import 'package:sizer/sizer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp(
  ));
}

class MyApp extends StatelessWidget {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, screenType) {
      return MaterialApp(
        initialRoute: '/',
        routes: {
          // When navigating to the "/" route, build the FirstScreen widget.

          // When navigating to the "/second" route, build the SecondScreen widget.
          '/RestaurantPage': (context) =>  Restaurants(),
          '/HomePage': (context) =>  AppBars(),
        },
        debugShowCheckedModeBanner: false,
        home: AppBars(),
      );
    });
  }
}

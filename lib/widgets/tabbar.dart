import 'package:flutter/material.dart';
import 'package:search_items/categories/all.dart';
import 'package:search_items/categories/hotels.dart';
import 'package:search_items/categories/park.dart';

import 'package:search_items/categories/restaurants.dart';
import 'package:sizer/sizer.dart';

class ProfileTabBarViewWidget extends StatefulWidget {


  @override
  State<ProfileTabBarViewWidget> createState() => _ProfileTabBarViewWidgetState();
}

class _ProfileTabBarViewWidgetState extends State<ProfileTabBarViewWidget>
    with SingleTickerProviderStateMixin {
  late TabController _controller;


  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 5, vsync: this);
    _controller.addListener(_handleTabSelection);
  }
  void _handleTabSelection() {
    setState(() {
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 100.0.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,

        children: [

          Container(
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey))),
            child:  TabBar(
                indicatorColor: Colors.blue,
                controller: _controller,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey,
                tabs:  [
                  Tab(
                    icon: Icon(
                        Icons.all_out_outlined,
                        color: _controller.index==0
                            ?  Colors.black
                            : Colors.grey
                    ),
                    text: "All",

                  ),
                  Tab(
                    icon: Icon(
                        Icons.restaurant,
                        color: _controller.index==1
                            ?  Colors.black
                            : Colors.grey
                    ),
                    text: "Eatery",
                  ),

                  Tab(
                    icon: Icon(
                        Icons.park_rounded,
                        color: _controller.index==2
                            ?  Colors.black
                            : Colors.grey
                    ),
                    text: "Park",),
                  Tab(
                    icon: Icon(
                        Icons.hotel,
                        color: _controller.index==3
                            ?  Colors.black
                            : Colors.grey
                    ),
                    text: "Hotels",),
                  Tab(
                    icon: Icon(
                        Icons.more_outlined,
                        color: _controller.index==4
                            ?  Colors.black
                            : Colors.grey
                    ),
                    text: "...More",),
                ]),
          ),

          Expanded(
            child: TabBarView(controller: _controller, children:  [
              All(),
              Restaurants(),
              Parks(),
              Hotels(),
              Text("abc")


            ]),
          ),
        ],
      ),
    );

  }
}
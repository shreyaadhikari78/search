import 'package:flutter/material.dart';

import 'package:search_items/widgets/tabbar.dart';
import 'package:sizer/sizer.dart';

class AppBars extends StatefulWidget {
  @override
  _AppBarsState createState() => _AppBarsState();
}

class _AppBarsState extends State<AppBars> with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 5, vsync: this);
    _controller.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ProfileTabBarViewWidget(),
    );
  }
}
